## Setting up and running

Last LTS release of nodejs is required (by react-scripts). Project follows usual conventions: `npm install` will install dependencies, `npm start` will run both backend and frontend part, `npm test` runs test.

## Architecture

### Overview

Code is written in TypeScript, because it's safer and because DX matters. Also it's the single thing that makes React palpatable for me:)
Usually I use way more stricter linter rules, but since create-react-app doesn't really allow to customise eslint I didn't bother to add them.

### Core 
Core is located in `src/lib/t9.ts`. Implementation uses recursion to get permutations and as such is probably more slow and memory consuming than real-life counterparts, although I haven't really noticed any bottlenecks. Possible way to improve this would be using trie with proper dictionary (but not before running performance tests).
The core is isomorphic and parts of it are used both at backend and frontend. It's covered with unit tests.

### Frontend
Frontend is scaffolded with create-react-app. I intentionally didn't use any state management library (but if I would, I'd went with MobX), because it would just bloat the code with no real value; there's only 3 values in state and component hierarchy is only 3 levels deep. This is also the reason for not writing any tests for frontend code.
In larger project I'd probably write styles using CSS Modules, but it requires non-trivial integration with TypeScript.
In larger project with more backend endpoints I'd use axios instead of Fetch API.


### Backend
Backend uses express.js. It consists of single file; I didn't see the need to split single API endpoint into separate file. It's launched by nodemon in dev environment.

## What I learned

* How T9 worked in old phones:)
* How to launch TS project from nodemon
* create-react-app is still really underwhelming, poor both in features and customisation

export interface IDict<T> {
    [key: string]: T;
}

type KeypadMap = IDict<string[]>;

export const KEYPAD_MAP: KeypadMap = {
    2: 'abc'.split(''),
    3: 'def'.split(''),
    4: 'ghi'.split(''),
    5: 'jkl'.split(''),
    6: 'mno'.split(''),
    7: 'pqrs'.split(''),
    8: 'tuv'.split(''),
    9: 'wxyz'.split(''),
};

export const PROPER_KEYS = Object.keys(KEYPAD_MAP);

export function t9(rawInput: string): string[] {
    return getWords(prepareInput(rawInput.trim()));
}

function prepareInput(input: string): string[] {
    return input.split('').filter(char => PROPER_KEYS.includes(char));
}

function mapInput(chunks: string[]): string[][] {
    return chunks.map(char => KEYPAD_MAP[char]);
}

function getWords(chunks: string[]): string[] {
    const availableChars = mapInput(chunks);

    if (availableChars.length === 0) {
        return [];
    } else if (availableChars.length === 1) {
        return availableChars[0];
    } else {
        return getPermutations(availableChars);
    }
}

/**
 * This is the heart of our logic, where permutations are made.
 * It works very similarly to how usually Fibonacci generators are written.
 * So it's pretty slow and consumes a lot of memory. Add memoization/trie/redis cache before really using it.
 */
function getPermutations(list: string[][]): string[] {
    const [head, ...tail] = list;
    if (tail.length === 1) {
        return prependList(head, tail[0]);
    } else {
        return prependList(head, getPermutations(list.slice(1)));
    }
}

function prependList(head: string[], list: string[]): string[] {
    return head.reduce<string[]>((acc, char) => {
        acc.push(...prepend(char, list));
        return acc;
    }, [])
}

function prepend(head: string, tail: string[]): string[] {
    return tail.map((char) => head + char);
}
import {t9} from './t9';

it('return correct amount of permutations', () => {
    const result = t9('234');
    expect(result.length).toBe(27);
});

it('returns correct permutations', () => {
    const result = t9('23');
    expect(result).toEqual(['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']);
});

it('handles incorrect chars without a crash', () => {
    const result = t9('1*#');
    expect(result).toEqual([]);
});
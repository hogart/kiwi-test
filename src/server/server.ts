import express from 'express';
import dotenv from 'dotenv';

import {t9} from '../lib/t9';

dotenv.config({path: '../../.env'});
const port = process.env.SERVER_PORT || 9090;

const app = express();

app.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/api/t9', (req, res) => {
    if (!req.query.input) {
        return res.status(400).send();
    } else {
        return res.send(t9(req.query.input));
    }
});

app.listen(port, () => {
    console.log(`Backend is listening at ${port}`);
});
import React, {Component} from 'react';
import {IDict, KEYPAD_MAP, PROPER_KEYS} from '../lib/t9';
import {PhoneButton} from './PhoneButton';
import './PhoneKeypad.scss';

const fullKeypad: IDict<string[]> = {
    ...KEYPAD_MAP,
    1: [''],
    0: ['_'],
    '*': ['🡄'],
    '#': ['🗙'],
};

const keypadOrder = [
    1, 2, 3,
    4, 5, 6,
    7, 8, 9,
    '*', 0, '#',
];

interface PhoneKeypadProps {
    onDigit: Function;
    onBackSpace: Function;
    onClear: Function;
    onSpace: Function;
}

export class PhoneKeypad extends Component<PhoneKeypadProps> {
    private onKey = (char: string | number) => {
        if (PROPER_KEYS.includes(char.toString())) {
            this.props.onDigit(char);
        } else if (char === '*') {
            this.props.onBackSpace();
        } else if (char === '#') {
            this.props.onClear();
        } else if (char === 0) {
            this.props.onSpace();
        }
    };

    render() {
        return <div className="PhoneKeypad">
            {keypadOrder.map((key) => {
                return <PhoneButton
                    key={key}
                    digit={key}
                    chars={fullKeypad[key]}
                    onClick={this.onKey}
                />
            })}
        </div>
    }
}
import React, { Component } from 'react';
import './PhoneButton.scss'

interface PhoneButtonProps {
    digit: number | string;
    chars: string[];
    onClick: Function;
}

export class PhoneButton extends Component<PhoneButtonProps> {
    private onClick = () => {
        this.props.onClick(this.props.digit);
    };

    render() {
        return <button className="PhoneButton" onClick={this.onClick}>
            <strong>{this.props.digit}</strong>
            <span>{this.props.chars.join('')}</span>
        </button>
    }
}
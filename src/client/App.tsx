import React, {Component} from 'react';
import './App.scss';
import {PhoneKeypad} from './PhoneKeypad';
import {fetchJSON} from './fetchJSON';
import {AvailableWords} from './AvailableWords';

interface AppProps {
}

interface AppState {
    input: string;
    words: string[];
    message: string;
}

class App extends Component<AppProps, AppState> {
    constructor(props: any) {
        super(props);
        this.state = {
            input: '',
            words: [],
            message: '',
        }
    }

    private onDigit = (digit: string) => {
        this.setState({
            input: (this.state.input + digit).trim(),
        });
    };

    private onInput = (event: any) => {
        this.setState({
            input: (event.currentTarget.value).trim(),
        });
    };

    private async onStateChange() {
        let words: string[] = [];
        if (this.state.input.length > 0) {
            words = await fetchJSON<string[]>('http://localhost:9090/api/t9?input=' + this.state.input);
        }

        this.setState({
            words,
        })
    }

    private onWord = (word: string) => {
        this.setState({
            message: this.state.message + word,
        });
    };

    async componentDidUpdate(prevProps: Readonly<AppProps>, prevState: Readonly<AppState>, snapshot?: any) {
        if (this.state.input !== prevState.input) {
            let words: string[] = [];
            if (this.state.input.length > 0) {
                words = await fetchJSON<string[]>('http://localhost:9090/api/t9?input=' + this.state.input);
            }
            this.setState({
                words,
            });
        }
    }

    private clearMessage = () => {
        this.setState({
            input: '',
            message: '',
        });
    };

    private backSpace = () => {
        const msg = this.state.message;
        this.setState({
            message: msg.substring(0, msg.length - 1),
        });
    };

    private addSpace = () => {
        this.setState({
            message: this.state.message + ' ',
            input: '',
        })
    };

    render() {
        return (
            <div className="App">
                <label>
                    <p>Compose a message:</p>
                    <textarea value={this.state.message} readOnly/>
                </label>
                <AvailableWords words={this.state.words} onWordSelect={this.onWord}/>
                <PhoneKeypad
                    onDigit={this.onDigit}
                    onBackSpace={this.backSpace}
                    onClear={this.clearMessage}
                    onSpace={this.addSpace}
                />
                <input className="DebugInput" type="text" title="Debug output" value={this.state.input} onChange={this.onInput}/>
            </div>
        );
    }
}

export default App;

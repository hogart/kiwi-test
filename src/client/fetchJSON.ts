export function fetchJSON<TResult>(url: string, init?: object): Promise<TResult> {
    return fetch(url, init)
        .then((response) => response.json());
}
import React, {Component} from 'react';
import './AvailableWords.scss';

interface AvailableWordsProps {
    words: string[];
    onWordSelect: Function;
}

export class AvailableWords extends Component<AvailableWordsProps> {
    render() {
        return <div className="AvailableWords">
            {this.props.words.map((word, index) => {
                return <span key={index} onClick={() => this.props.onWordSelect(word)}>{word}</span>
            })}
        </div>
    }
}